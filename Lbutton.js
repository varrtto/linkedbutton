import React from 'react';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const Lbutton = (props) => {
  return (
    <Link to={props.to} style={{textDecoration: 'none'}} >
      <Button variant={props.variant}>
        {props.children}
      </Button>
    </Link>
  )
}

export default Lbutton;
